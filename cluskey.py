#!/usr/bin/env python3

#*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
#      _           _                            
#  ___| |_   _ ___| | _____ _   _   _ __  _   _ 
# / __| | | | / __| |/ / _ \ | | | | '_ \| | | |
#| (__| | |_| \__ \   <  __/ |_| |_| |_) | |_| |
# \___|_|\__,_|___/_|\_\___|\__, (_) .__/ \__, |
#                           |___/  |_|    |___/ 
#
#*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

# This implementation of McCluskey is going to be a two-step operation to find
# (one of) the shortest possible function form(s) to a list of min/max terms as
# well as number of variables, and optionally a list of don't-cares.

# I will attempt to make it depend on as little modules as possible.

import re


def inputCheck(inp):
    # input is exactly like this:
    # f(comma,separated,variables) = m/M(1,2,3,...) d(4,5,6,...)
    # case-sensitive. User must be warned interactively.
    # using regex to check is the cleanest way I can think of.
    
    pat = re.compile(r"f *\((\w,)+\w\) *=( *[mMdD] *\((\d,)+\d\))+")
    match = re.fullmatch(pat,inp)

    return bool(match)


